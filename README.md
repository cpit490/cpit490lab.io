# CPIT-490 course website
This is the static website of CPIT490, Systems and Cloud Adminstration. 

## Build Instruction

1. [Install Hugo](https://gohugo.io).
2. Clone this repo.
3. Serve locally: `hugo server -D` -D is to show draft posts.
