---
title: "Vagrant"
date: 2022-11-01T19:32:30+03:00
draft: false
---

In this lecture note, you will create your first virtual machine with _Vagrant_. It will provide you with a quick introduction on how to use Vagrant, its prerequisites, and an overview of three of the most important Vagrant commands: `vagrant init`, `vagrant up`, and `vagrant ssh`.

## Install Vagrant
- To install Vagrant go to [vagrantup.com](https://www.vagrantup.com/) and install it on your machine.

## Install a Vagrant Provider
Vagrant Providers are the hypervisors that Vagrant uses to set up and create virtual environments. Vagrant supports multiple providers including VirtualBox, VMware, Hyper-V, and [many others](https://developer.hashicorp.com/vagrant/docs/providers).
- Install [VirtualBox](https://www.virtualbox.org/) if you do not have it.
  - macos users: if you're on Apple Silicon CPUs, then you may install [VMware Fusion Tech Preview](https://customerconnect.vmware.com/downloads/get-download?downloadGroup=FUS-PUBTP-22H2) or [Parallels](https://www.parallels.com/products/desktop/).

## Install a Vagrant provider plugin
If you're using VirtualBox as the hypervisor. Vagrant ships out of the box with support for VirtualBox, so no additional tools or plugins are needed. However,  if you would like to use VirtualBox Guest Additions on the guest OS, then install the _vagrant-vbguest_  plugin:
  ```bash
  vagrant plugin install [vagrant-vbguest](https://github.com/dotless-de/vagrant-vbguest)
  ```
- If you're using VMware as the hypervisor, then install the [Vagrant VMware plugin](https://developer.hashicorp.com/vagrant/downloads/vmware):
  ```bash
  vagrant plugin install vagrant-vmware-desktop
  ```
- If you're using Parallels as the hypervisor, then install the [Vagrant Parallels plugin](https://parallels.github.io/vagrant-parallels/):
  ```bash
  vagrant plugin install vagrant-parallels
  ```  
  
## Install a Vagrant box

A Vagrant box is simply the operating system image for the Vagrant environment. Vagrant boxes are all provider-specific. For example, a box for VirtualBox is not going to work with the VMware Fusion provider, or any other provider.

We will use the ubuntu 22.04 virtual machine image provided by the [bento project](https://github.com/chef/bento). This vagrant box can be founded on [Vagrant cloud](https://app.vagrantup.com/bento/boxes/ubuntu-22.04). You can find more boxes at HashiCorp's [Vagrant Cloud box catalog](https://app.vagrantup.com/boxes/search).


```bash
vagrant init bento/ubuntu-22.04
```

If you're using multiple providers, then you need to specify the provider you're going to use with the Vagrant box. For example, if you're using VMware, you will run:

```bash
vagrant init bento/ubuntu-22.04 --provider=vmware
```

This will create a Vagrantfile in the current working directory. Open the `Vagrantfile` in your text editor, and uncomment the shell script at the bottom of the file (simply remove the `#` character), so it looks like the code below.

```
  config.vm.provision "shell", inline: <<-SHELL
    apt-get update
    apt-get install -y apache2
    apt-get install php php-mysql
  SHELL
```

> **Note:** The Vagrantfile is written in Ruby, so you do not have to worry about whitespace indentations although it is always good to keep hitespace indentations consistent for readability.


### Start the Vagrant box

To start the Vagrant box, run:

```bash
vagrant up
```

### Connect to Vagrant box

Vagrant runs the virtual machine without a UI. To connect to Vagrant box via SSH, run:

```bash
vagrant ssh
```

To leave the SSH session, run `logout`.


### Destroy the Vagrant box

To  terminate the virtual machine, run:
```bash
vagrant destroy
```

This command will not delete the virtual machine (i.e., the downloaded box file), to delete it run:

```bash
vagrant box remove bento/ubuntu-22.04
```


### Listing available Vagrant boxes

To list the available vagrant boxes on your environment, run:

```bash
vagrant box list
```

### Share a Vagrant box

Vagrant share is a command that allows you to share your Vagrant box with anyone in the world with just a single command. To use this feature, you will need to install the _Vagrant share plugin_ via

```bash
vagrant plugin install vagrant-share
```

To share a vagrant box, run:

```bash
vagrant share
```