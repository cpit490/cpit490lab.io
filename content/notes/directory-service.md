---
title: "Directory Service"
date: 2023-03-03T00:05:55+03:00
draft: true
description: "Open LDAP"
---


Steps:
1. 
2. 
3. 


> This installation instructions were tested on Ubuntu Server 22.04. If you're using another Linux distribution, the instructions may vary slightly but the same concepts should apply.


Before going through the configuration steps, it's worth reviewing basic networking concepts.


### Private IPv4 Address, Public IPv4 Address, and NAT
As public IPv4 addresses become scarce and depletable resources, technologies such as Network Address Translation (NAT) were introduced to conserve public IPv4 addresses. NAT enables ISPs to assign one publicly routable IP address to the gateway device (e.g., router or modem) and hide hall network devices behind private non-routable IP addresses instead of allocating a public address to each network device. When you get a router for your home network, your Internet Service Provider (ISP) assigns it a public IP address that is routable to the public Internet. Your router assigns all connected devices a non-routable private IPv4 address and creates a NAT table to map incoming traffic to the public IPv4 to private IPs.

NAT is a method of mapping or redirecting an IP address space into another space. It's often used between a private LAN and the public Internet. Specifically, NAT translates a private IP address into its public IP address for outgoing traffic and translates a public IP address to the corresponding internal private IP address for incoming traffic. NAT conserves IPv4 address space and hides the internal IP address of a host on a private network.

A host that has public IP address can provide services (e.g. web server) and make them accessible directly from the rest of the Internet.

Address ranges to be use by private networks are:

- Class A: **10**.0.0.0 to **10**.255.255.255.
- Class B: **172.16**.0.0 to **172.31**.255.255.
- Class C: **192.168**.0.0 to **192.168**.255.255.

## Virtualization

We will need to have four Virtual Machines (VMs) running in a hypervisor (VirtualBox)
- Debian Server with no Desktop/GUI environment
- Debian Desktop with a Desktop environment
- AlmaLinux Server (or any binary-compatible Linux distribution with   vb  ojRed Hat Enterprise Linux (RHEL). such as RockyOS)
- Windows 10 Pro [Optional]

The network for these VMs should be configured in NAT mode and not in bridge mode because we wanted these VMs to communicate with each other.





### Configuring a NAT network in VirtualBox

In VirtualBox, go to network settings (under Tools) -> Netowkr

## Ubuntu Server VM
```bash
sudo apt install network-manager
```

### AlmaLinux VM

```bash
ip address show
```

if you do not see an IPv4 that starts with 10.0.2, then you need to assign it manually using

```bash
nmcli con mod enp0s3 ipv4.addresses 10.0.2.5/24
```
to save changes run:

```bash
nmcli con up enp0s3
```

To confirm the IP, once again run the command:
``bash
ip address show enp0s3
```


### Windows VM

Running `ipconfig` in the Windows VM shows an IP address of `10.0.2.15`.
## DHCP


## Directory Service


Prepare your server by updating existing packages:

```bash
sudo apt update
```

to install LDAP directory server, we need the following packages:

| Package | Description |
|---------|-------------|
| ldap-utils | contains the services and utilities to configure and run an LDAP server, to run the OpenLDAP server and client. |
| slapd | contains both  including the Standalone LDAP Daemon, `slapd`

To install these packages using `apt`:

```shell
sudo apt -y install slapd ldap-utils
```





During  installation you will be asked to enter LDAP admin password:

![](/images/notes/ldap/ldap-admin-password.png)

Enter and confirm the password and continue installation by selecting **<ok>** with TAB key.

At the time of writing `openldap-servers` has no pre-build binary packages for CentOS/RockyLinux/AlmaLinux, so we need to install it manually

Once installation is successful, start the LDAP directory server process, which is called `slapd`.




We also need to use LDAP Migration Tools, which are a collection of Perl scripts used for converting configuration files to the LDIF format making these files compatible with our LDAP Server.



To download the LDAP Migration Tools and get more information, go to the following address: http://www.padl.com/tools.html .
and migrating users, groups, aliases, hosts, netgroups, networks, protocols, RPCs, and services from existing name services.

Install _migrationtools_

