---
title: "Proxy Caching Server"
date: 2022-12-10T14:20:31+03:00
draft: true
---

# Proxy Servers
A proxy server is a server application that acts as an intermediary between a client requesting a resource and the server providing the resource. We will look at two uses of proxy servers as a **caching proxy server** and as a **forwarding proxy server**.

## Caching Web Server

A caching web server is a system for speeding up web access by caching repeated requests. A cache is simply a storage area that stores data for a short period of time. While most modern browsers provide caching,


## HTTP Web Proxy
An HTTP web proxy is an intermediary server for forwarding HTTP traffic.


## SOCKS5 Proxy


SOCKS is a layer 5 (session layer) Internet protocol that exchanges network packets between a client and server through a proxy server. ASOCKS server accepts incoming connection on TCP port 1080  authentication and adds support for IPv6 and UDP, caching 