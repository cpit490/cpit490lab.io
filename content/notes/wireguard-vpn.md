---
title: "Wireguard VPN"
date: 2024-04-29T18:22:39+03:00
draft: false
---

WireGuard is a modern, secure, efficient, high-performance VPN protocol. It's often considered more simpler and efficient than IPsec and OpenVPN. In this lecture note, we will set up a wireguard VPN server on the cloud and restrict access to our resources on the cloud to VPN only.

## Requirements
- Provision a Virtual Machine (VM) with at least 1GB of RAM.
- Allow TCP port 22 for SSH and UDP port 51820 for WireGuard.
- We will use [Algo](https://github.com/trailofbits/algo), a set of Ansible scripts that simplify the setup of a WireGuard VPN.

## Step 1: Create a VM instance for the VPN server
1. Create a Linux (Ubuntu server) VM instance (1GB of memory is required) and generate your SSH key pair using `ssh-keygen -t rsa`.
2. In your cloud provider dashboard, go to the network settings and find the option that controls which ports are open to the public (security groups). Open the following inbound ports:
  - Protocol: **UDP**, Port: **51820** (for VPN)
  - Protocol: **TCP**, Port: **22** (for SSH)
3. Log in to your VM instances using SSH.

```bash
ssh -i /path/to/your/private/key/file azureuser@public-ip-or-DNS-name
```

- Type the _passphrase_ you entered when generating the SSH key pair and you should be logged into your remote VM instance.
- Update and upgrade installed packages to ensure we have the latest software packages and apply any security fixes

```bash
sudo apt update
sudo apt upgrade -y
```

- Make sure you have *python 3.10* or above:

```bash
python3 --version
```
- Install the `python3-venv` packag

```bash
sudo apt install python3.10-venv -y
```

## Step 2: Clone Algo and Algo's remaining dependencies

```bash
git clone https://github.com/trailofbits/algo.git
cd algo
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

## Step 3: Configure and install WireGuard using Algo

- Open the file config.cfg in your favorite text editor. Specify the users you wish to create in the users list. Create a unique user for each device you plan to connect to your VPN.

```bash
nano config.cfg
```

- Install wireguard using algo

```bash
./algo
```

```
[Cloud prompt]
What provider would you like to use?
    1. DigitalOcean
    2. Amazon Lightsail
    3. Amazon EC2
    4. Microsoft Azure
    5. Google Compute Engine
    6. Hetzner Cloud
    7. Vultr
    8. Scaleway
    9. OpenStack (DreamCompute optimised)
    10. CloudStack (Exoscale optimised)
    11. Linode
    12. Install to existing Ubuntu latest LTS server (for more advanced users)
```
- Enter 12 for the Option "12. Install to existing Ubuntu latest LTS server (for more advanced users)"

```
Do you want macOS/iOS clients to enable "Connect On Demand" when connected to cellular networks?
[y/N]
N
[Wi-Fi On Demand prompt]
Do you want macOS/iOS clients to enable "Connect On Demand" when connected to Wi-Fi?
[y/N]
N
```

- VPN On Demand Rules on macOS: You may select yes or no ([y/N]) for VPN On Demand on macOS, which enables automatic control of a VPN connection based on specific criteria such as auto connecting to VPN when on WiFi and auto disconnecting when on cellular. For more info, [see this article](https://support.apple.com/en-sa/guide/deployment/depae3d361d0/web).

- Enter *y* (yes) for the question "Do you want to retain the keys (PKI)?". This is because we may need to additional users in the future.

- Enter *N* (No) for DNS Ad blocking since this VPN is used to restrict access to compute resources.

```
[DNS adblocking prompt]
Do you want to enable DNS ad blocking on this VPN server?
[y/N]
N
```

- Enter *N* (No) for SSH tunneling prompt. SSH tunneling is a separate feature that allows us to tunnel arbitrary network traffic over the SSH connection. Our goal is to restrict SSH access to our VMs to connections from the VPN server not to tunnel our internet traffic through that connection. This feature may be helpful when using a non-encrypted protocol but SSH is encrypted, so we do not need to increase the load on the VPN server.

```
[SSH tunneling prompt]
Do you want each user to have their own account for SSH tunneling?
[y|N]
N
```

- Next, hit Enter to proceed

```
Local installation might break your server. Use at your own risk.

Proceed? Press ENTER to continue or CTRL+C and A to abort...:
```

- The first IP address is the local IP address, you leave that as `localhost`, so hit ENTER to select the default value.

```
[local : pause]
Enter the IP address of your server: (or use localhost for local installation):
[localhost]
:
```

- The second IP address should be the public IP address of the server to verify the certificate. You should copy this from the Azure portal:

```
[local : pause]
Enter the public IP address or domain name of your server: (IMPORTANT! This is used to verify the certificate)

```

- Enter your user name (e.g., azureuser). The root user is typically not allowed to log in via SSH on Azure VMs for security reasons.

```
What user should we use to login on the server? (note: passwordless login required, or ignore if you're deploying to localhost)
[root]
azureuser
```

- You should have successfully completed the installation:


```
ok: [localhost] => {
    "msg": [
        [
            "\"#                          Congratulations!                            #\"",
            "\"#                     Your Algo server is running.                     #\"",
            "\"#    Config files and certificates are in the ./configs/ directory.    #\"",
            "\"#              Go to https://whoer.net/ after connecting               #\"",
            "\"#        and ensure that all your traffic passes through the VPN.      #\"",
            "\"#                     Local DNS resolver 172.17.109.143                   #\"",
            ""
        ],
        "    \"#        The p12 and SSH keys password for new users is Yaiu0hs44       #\"\n",
        "    \"#        The CA key password is MuT0i0IdMWRs_Hgd       #\"\n",
        "    "
    ]
}

```

- Show the current state of all active WireGuard interfaces on the server.

```bash
sudo wg show
```

## Step 4: Configure the VPN Client

- End the SSH connection and download the configuration file from the server to your local machine. The generated config file should be at `/home<username>/algo/configs/<server_ip_address>/wireguard/<client_name>.conf`. We need to download it to our client machine, then setup a new connection with it.

```
exit
scp -i <private_ssh_key> <azureuser>@<public_ip>:<absolute_path_on_the_server> <target_directory_on_the_client>
```

- Install the [WireGuard VPN Client](https://www.wireguard.com/install/) for your operating system. Import the downloaded `.conf` file to your WireGuard VPN client, then setup a new connection with it.


## Step 5: Restrict SSH access to VMs to only VPN

- Create another VM in the same region and launch it.
- Go to **Networking** --> **Network settings** --> **Network security group**
- Add a network security group

![](/images/notes/wireguard-vpn/azure-ssh-access-source-ip.png)

- Verify the server's SSH access by attempting to connect both with and without the VPN client connected.

## Tearing down all resources

You can now tear down the VMs, storage, public IPs, VNet/VPC after the completion of this lecture note/in-class activity to prevent ongoing costs.


###
