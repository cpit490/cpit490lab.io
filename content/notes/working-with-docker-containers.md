---
title: "Working With Docker Containers"
date: 2018-11-23T16:54:59+03:00
draft: false
---

In the previous lecture, we [introduce Docker containers](/notes/docker-containers/)and saw how to dockerize an app. We also saw how to [create and work with images](/notes/working-with-docker-images). In this lecture, we will see how to work with containers and learn the basics of interacting with Docker.


### Pre-requisites:
Make sure that _docker_ is installed and functional:

```bash
docker info
```

```
Containers: 41
 Running: 0
 Paused: 0
 Stopped: 41
Images: 47
Server Version: 17.03.1-ce
Storage Driver: aufs
 Root Dir: /var/lib/docker/aufs
 Backing Filesystem: extfs
 Dirs: 132
 Dirperm1 Supported: true
Logging Driver: json-file
Cgroup Driver: cgroupfs
. . .
. . .
Username: kalharbi
Registry: https://index.docker.io/v1/
Experimental: true
Insecure Registries:
 127.0.0.0/8
Live Restore Enabled: false
```


## Launching a Container
- Before we launch a container, we need to select a base Docker image to create a container from. The most common way to find container images is to search the [Docker Hub](https://hub.docker.com/) for images. We can also search the Docker hub from the command line using the _docker_ command line tool with the search command. For example, we can search for the _alpine_ docker image, which is a minimal Docker image based on Alpine Linux and very small in size (around 5 MB).

    ```bash
    docker search alpine
    ```
    ```
    NAME                                   DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
    alpine                                 A minimal Docker image based on Alpine Lin...   4622      [OK]       
    mhart/alpine-node                      Minimal Node.js built on Alpine Linux           403                  
    anapsix/alpine-java                    Oracle Java 8 (and 7) with GLIBC 2.28 over...   370                  [OK]
    ...
    gliderlabs/alpine                      Image based on Alpine Linux will help you ...   178                  
    frolvlad/alpine-glibc                  Alpine Docker image with glibc (~12MB)          170                  [OK]
    alpine/git                             A  simple git container
    running in alpine ...   57                   [OK]
    ```

- Next, we will launch our first Docker container from the latest version of the _alpine_ docker image.

    ```bash
    docker run -i -t alpine /bin/sh
    ```
    ```
    docker run -i -t alpine:latest /bin/sh Unable to find image 'alpine:latest' locally
    3.7: Pulling from library/alpine
     c67f3896b22c: Pull complete 
     Digest: sha256:a52b4edb6240d1534d54ee488d7cf15b3778a5cfd0e4161d426c550487cddc5d
    Status: Downloaded newer image for alpine:latest
    / # 
    ```
So let's see what happened:
   
  - We asked _docker_ to run the default shell (/bin/sh) in a container from the alpine image.
       - If the image is not available locally, docker will download it from the docker hub registry. Alternatively, you may download an image using `docker pull alpine` prior to executing `docker run`, and the run command will use that image.
       - Note that _alpine Linux_ is based on _busybox_, which uses a lightweight Unix shell called Dash (Almquist shell). So keep in mind that this is a different shell implementation and may have differences with the Bash shell you're familiar with.
  - We used two command line options `-i` and `-t` to connect to an interactive shell in the container.
       - The `-i` option keeps _STDIN_ open from the container.
       - The `-t` option allows Docker to assign a pseudo-tty to the container to emulate a terminal.

- This container is running the shell `/bin/sh` and we did not exit the shell. To see the list of running containers, execute the following command in the host:
     
    ```bash
    $ docker ps
    ```
    ```
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
    d5d1051ecdd2        alpine              "/bin/sh"           14 seconds ago      Up 10 seconds                           eager_pike
    ```

## Terminating a Container
A container is terminated when your running command exits. That's it, the container is running as long as the command you launched the container with is running.

- Try to exit from the shell in your container using the command `exit`. Then, run the following command in the host: 
     
    ```bash
    $ docker ps
    ```
    ```
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
    ```
    - The `docker ps` command showed no results because it defaults to listing **running containers**, and our container has terminated.
- To see all containers including **stopped containers**:
    ```bash
    docker ps -a
    ```
    ```
    CONTAINER ID        IMAGE                                     COMMAND                  CREATED             STATUS                      PORTS               NAMES
    d5d1051ecdd2        alpine                                    "/bin/sh"                9 minutes ago       Exited (0) 6 minutes ago                        eager_pike
    ```
- You can keep a container running by 
## Container Naming
Docker will assign a name at random for each container. In the example above, Docker assigned the name _eager_pike_ to our container. We can specify a particular name for the container using the `--name` flag.
  
  - Run and name the container:
    ```bash
    $ docker run -i -t --name my-alpine-container alpine /bin/sh
    ```
  - Exit the shell using the `exit` command.

## Starting a stopped container
We can start a stopped container using the `docker start <container_name>` command:
    ```bash
    $ docker start -i my-alpine-container
    ```
   - The `-i` flag is to attach the STDIN of the host to the container's STDIN.
   - Keep this container running and do not exit.

## Attaching to a running container
We can attach to a running container using the command `docker attach <container_name>`, which will attach local standard input, output, and error streams to a running container. Example:
    ```bash
    $ docker attach my-alpine-container
    ```

## Stopping a running container
We can stop one or more running containers using the command `docker stop <container_name>`. Example:
    ```bash
    $ docker stop my-alpine-container
    ```

## Deleting a container
We can delete a container when we are finished with it using the command `docker rm <container_name_or_id>`. Example:
    ```bash
    $ docker rm my-alpine-container
    ```
- We can also delete a container by its id:
   
    ```bash
    $ docker ps -a
    ```
    ```
    CONTAINER ID        IMAGE                                     COMMAND                  CREATED             STATUS                      PORTS               NAMES
    991c16ddcb47        alpine                                    "/bin/sh"                30 seconds ago      Exited (0) 28 seconds ago                       my-alpine-container
    ```
    ```bash
    $ docker rm 991c16ddcb47
    ```
- To ensure that the container has been deleted, we can list all available containers:
   
    ```bash
    $ docker ps -a
    ```
    ```
    CONTAINER ID        IMAGE                                     COMMAND                  CREATED             STATUS                      PORTS               NAMES
    ```
- We can see above that the container is no longer available.
