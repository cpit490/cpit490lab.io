---
title: "Introduction to System Administration"
date: 2024-01-28T10:12:40+03:00
draft: false
---

## What is System Administration?

System administration refers to managing the operation of computer systems in a multi-user environment and the tasks and responsibilities involves in maintaining, monitoring, and supporting the operation of these systems.

## Roles and Responsibilities of a System Administrator

- Installing, configuring, and maintaining software and hardware
- Managing user accounts
- Fixing and and troubleshooting issues
- Monitoring system performance
- Keeping the system secure
- Backing up and restoring files
- Documenting the configuration of the system

## Linux

Linux is a free and open-source operating system created by Linus Torvalds in 1991. It is based on the Unix operating system.

### Linux Distribution
A Linux distribution, which is often abbreviated as distro, is an operating system made from the linux kernel and a collection of software packages. Examples of popular Linux distros include Debian, Ubuntu, openSUSE,, and AlmaLinux OS, which is binary compatible with Red Hat Enterprise Linux (RHEL).

### Linux Shell

A shell in Linux is a command-line interface that provides a user interface for access to an operating system's services. It takes commands from the keyboard and gives them to the operating system to execute them and returns the output back to the shell.

There are different types of shells in Linux including Bash (Bourne Again SHell), ZSH (Z Shell), CSH (C Shell), and KSH (Korn SHell).

## Basic Linux Commands

- `ls`: Lists all files and directories in the current directory
- `cd`: Changes the current directory
- `pwd`: Prints the path of the working directory
- `touch`: Creates a new file
- `cat`: Concatenates and prints files
- `rm`: Removes a file or directory
- `cp`: Copies a file or directory
- `mv`: Moves ore renames a file or directory

## Linux File System

In Linux, all data is stored in files, including hardware devices, processes, and directories. The Linux file system is hierarchical and starts at the root directory, denoted by a forward slash (`/`).

## Linux Permissions

In Linux, each file and directory has an owner, a group, and other users. Each of these categories has three permissions: read, write, and execute.

## Linux Processes

A process is the state of a running program. Linux has commands for managing processes, such as `ps` to display the currently running processes, `top` to display and update sorted information about processes, `&` to run a process in the background, and `kill` to terminate a process.

## Conclusion

System administration is a critical role in any organization, and Linux is a powerful tool for system administrators. By understanding the basics of Linux, you can manage and troubleshoot your system effectively.