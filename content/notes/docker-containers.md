---
title: "Introduction to Docker Containers"
date: 2018-09-12T07:25:35+03:00
draft: false
---

## What is Docker?

Docker is a platform for developing, deploying, and running applications in Linux containers. Docker is a popular tool for many reasons:

- __Containerization__: Operating system level virtualization.
- __Lightweight__: Containers are often smaller in size than virtual machines and share the host kernel.
- __Portable__: You can develop your app locally and deploy it on the cloud and run it anywhere where you can run Docker.
- __Manageable__: You can break a large monolithic application into a collection of small units, where each  unit runs in its own container. This technique is known as _Microservices_.
- __Scalable__: You can increase the number of containers and create identical replicas of them.

## Download and Install Docker
Please refer to the instructions on the Docker documentation at: [docs.docker.com/install](https://docs.docker.com/install/) to install Docker Community Edition (CE) on your platform. Docker works in a client-server architecture. You have a long-running background/daemon process (the `dockerd` command). and a docker client (the `docker` command) that communicates with the daemon process by passing requests to it .

## Docker CLI
You will use the `docker` command line interface (CLI) tool to work with Docker containers. It's recommended that you check the available commands and options using `docker --help`. To make sure that you have successfully installed and started the docker daemon is running, run:

```bash
docker info
```

This command should return common statistics (e.g., number of containers and images) and basic configuration.

```
Containers: 24
 Running: 0
 Paused: 0
 Stopped: 24
Images: 30
...

```

## Images vs Containers
A Docker image is an executable package that includes everything needed to run an application (OS, libraries, and configuration files). It is the building block that docker uses to build containers. A docker image is made up of file systems layered over each other.

A container is the running instance of an image. When an image is running, it's called a container. You can see a list of your running containers with the command, `docker ps`.

## Dockerfile
A _Dockerfile_ is a text document that contains all the commands or instructions to build a docker image. Below is a simple Dockerfile with two instructions:

```
FROM debian:12.5-slim
CMD ["echo", "Hello, World!"]
```

- `FROM debian:slim` This instruction sets the base image for the Docker container. In this case, the base image is `debian:slim`, which is a minimal Debian-based image.
- `CMD ["echo", "Hello, World!"]` This sets the default command that Docker will run when the container starts. In this case, it will print "Hello, World!" to the console.

When you build and run a Docker container from this Dockerfile, it will print out "Hello, World!" to the console and exit.

## Build your first Docker image
To build an image, you need to select a base image from a resource or a repository such as [hub.docker.com](https://hub.docker.com). Then, you may customize the image to your need.

- To build an image from the _Dockerfile_ shown above, run:

```bash
docker build -t hello-docker:v1 .
```
  - This will build a new image from the _Dockerfile_ with the name hello-docker and tag v1.
  - The `-t` option adds a name and a tag to the image in the 'name:tag' format.
  - The `.` argument points to the PATH that contains the _Dockerfile_, which is the present working directory.

- To run the created image, run:

```bash
$ docker run -i -t hello-docker:v1
```
  - This will run the command defined in the _Dockerfile_, which was running the bash command, in a new docker container.
  - The `-i` option will run in interactive mode to keep the STDIN open and the `-t` option will allocate a pseudo-TTY.


## Dockerize a Java application

1. Create a simple Java program with the following content:
   
      App.java
      
      ```java
      public class App {
      
        public static void main(String[]args){
          System.out.println("Hello Java!");
        }
      }
      ```
   - In order to compile Java files on a docker container, you need to have a container image with the Java Development Kit (JDK).
     - Search the [docker hub](https://hub.docker.com) for an image with _openjdk_. The search results show various images. We will pick the official `openjdk` image with the tag `8-jdk-alpine` because it's based on Alpine Linux, which is a minimal Docker image based on Alpine Linux with a very small size (around 5 MB).
         
     - The Java program can be compiled using the command `javac App.java`, which returns a compiled file named App.class that can be run using the command `java App`. We will issue these commands inside the container.
         
2. Create a file named _Dockerfile_ in the same directory that the above java code is in with the following content:
   
      ```Dockerfile
   from openjdk:8-jdk-alpine
   RUN mkdir /app
   COPY App.java /app
   RUN javac /app/App.java
   WORKDIR /app
   CMD ["java", "App"]
      ```

3. Build an image from the _Dockerfile_
   
      ```bash
    $ docker build -t java-container:latest .
      ```
4. Run the container
   
      ```bash
    $ docker run java-container:latest
      ```


## In-class Activity

Install Docker on your local machine or a VM instance and follow the steps above to dockerize the Java application. Submit a PDF file that contains screenshots of your screen.
