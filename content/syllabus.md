---
title: "Course Syllabus"
date: 2024-0-25T14:09:23+03:00
draft: false
toc: false
---

**Credits:** 3 credit hours
**Prerequisite:** CPIT-375

## Description
 
This course provides students with essential skills and hands-on experience on modern tools and technologies for administering development and deployment environments. This course discusses concepts essential to the administration of servers and introduces them into the recent technologies and infrastructures that run and manage modern software systems. Students who successfully complete this course will gain the essential knowledge and skills to work as a system administrator, DevOps engineer, or a site reliability engineer. This course uses UNIX and Linux operating systems to demonstrate various aspects of system administration. These operating systems powers over 94% of the world’s supercomputers and most of the servers powering the Internet. Students will gain a sufficient understanding of UNIX and Linux systems and be prepared to manage real world systems.

This course includes topics such as: configuring and deploying systems, troubleshooting system and network problems, shell scripting, virtualization, performance tracing and analysis, automation, and configuration management. 

## Course Learning Objectives
Upon successful completion of this course, students will be able to:

- Recognize the different administrative activities performed by a system administrator in an organization.
- Identify the structure of the UNIX file system and manage storage devices.
- Identify the components of a UNIX process and its life cycle.
- Demonstrate proficiency with the command line interface and common UNIX system management tools.
- Effectively plan, evaluate, perform, and schedule backup and recovery strategies.
- Effectively troubleshoot difficult and common system and networking problems.
- Effectively use virtualization technologies to utilize resources.
- Apply configuration management tools to automate system deployment and orchestrate changes across a number of machines.


## Textbook/References
- UNIX and Linux System Administration Handbook, 5th Edition,  Evi Nemeth, Garth Snyder, Trent R. Hein, Ben Whaley, Dan Mackin, Addison-Wesley Professional, August 18, 2017, ISBN-10: 0134277554, [website](https://admin.com/)
- Additional reading and video materials will be provided to students weekly.


## Course Outline

#### Module 1: Fundamentals of UNIX and LINUX systems
- Fundamentals of system administration: an overview
  - Essential roles of the system administrator.
  - History and background of UNIX and Linux systems.
  - UNIX and Linux Distributions
- Fundamentals of Unix and Linux systems
  - Installing, starting, and configuring software.
  - Shells and Command Line Interface (CLI) tools.
  - User account management
  - Backup and recovery
- The UNIX file system
  - The structure of the file system and its primitives
  - Storage management
  - Process management and scheduling.
- The Bash shell
  - Terminal navigation
  - File manipulation
  - Piping, streaming, and flow redirection
  - Searching and filtering
  - Program execution and package management
- The shell and shell scripting
  - Basic bash scripting
  - Python scripting

#### Module 2: Cloud Computing and App Deployment
- Cloud platform choices
  - Amazon Web Services, Google Cloud Platform, Microsoft Azure, and Digital Ocean.
  - Cloud service fundamentals: access, regions, virtual private server
  - Identity management and authorization
  - Cost control
- App deployment: setup and configuration
  - LAMP stack
  - Apache, PHP, and MySQL setup
  - Server configuration


#### Module 3: Networking and Virtualization
- Fundamentals of network administration
  - TCP/IP, DHCP configuration
  - Host configuration
  - VPN setup and configuration
  - Network troubleshooting
  - Firewall rules and configuration
  - Web hosting setup and configuration
- Virtualization
  - Native and cloud virtualization
  - Server Virtualization
  - Linux containers
  - Docker containers

#### Module 4: Performance Management
- Syslog and log files
  - Log management and rotation
  - Logging analysis and debugging
- Performance management
  - Performance metrics
  - Load balancing mechanisms
  - System performance analysis
- Performance tracing tools
  - Tracing sources
  - Performance profiling with perf, perf-tools, trace-cmd, and eBPF

#### Module 5: Software and configuration management
- Content management
  - Managing the software installed on the system for development and production environments
- Configuration management and automation
  - Configuration and scalability issues
  - Automation and configuration using Puppet
- Version Control
  - Git basics
  - Branching and merging
  - Remote repositories
  - Deploying and hosting a remote Git server


## Grading
- Assignments and in-class activities: 20%
- Midterm exam: 15%
- Group project: 20%
- 15% on:
  - Labs for CPIT-490 ONLY
  - Group presentation for CPIT-632 ONLY
- Final exam: 30%

## Late Submission Policy
Assignments submitted late will incur a 25% penalty. You may submit an assignment or a lab activity up to one week late. After that the submission will not be graded and you’ll receive 0 points for it. However, every student gets three free late passes, allowing you to submit a maximum of 3 assignments up to 1 week past the due date without penalty.

## Missed Exam Policy

If you find out that you are unable to attend the midterm or final exam, you will need to get in touch with me as soon as possible and provide necessary documentation, so we can make other arrangements with the approval of the department.

## Academic Integrity

As a student, you are expected to submit your own original work. You may not submit work written by others (human, tools, or generative AI) and claim it to be yours, or use "recycle" work prepared for previous courses without obtaining written permission from your instructor. Violations of academic integrity include, but are not limited to, cheating, plagiarism, falsifying data, knowingly assisting others in acts of academic dishonesty. Students who commit offenses of academic integrity will be reported to the deanship of academic affairs and will face disciplinary actions as per article 4 of KAU's Students’ Behavior Control Regulations (code of conduct). These actions could result in outcomes such as failure on the assignment or exam, failure in the course, suspension for one semester, or even expulsion from the university. KAU's code of conduct can be [downloaded using this link](https://www.kau.edu.sa/GetFile.aspx?id=313706&fn=Students%E2%80%99+Behavior+Control+Regulation+at+KAU+(code+of+conduct).pdf).

## Policy on the use of generative AI tools

This policy pertains to the use of generative Artificial Intelligence (AI) tools that are also called Large Language Models (LLMs) such as ChatGPT, Bard, GitHub CoPilot, Bing Chat, Claude and many others. It's important to note that this course emphasizes the acquisition of essential knowledge and the development of technical skills. Thus, it's expected that all coursework and assessments should be prepared by the student working individually or in groups as specified. **Students may not have another person or AI tools complete any substantive portion of the coursework or assessment.** Academic integrity is a core principle in academia and you're expected to uphold this principle. This applies to all work, regardless of whether or not you have used generative AI tools. However, you're allowed to experiment with these tools as long as they support your own learning, and they don't become shortcuts to dishonest work. A responsible use of generative AI tools in assigned course work or assessment must be approached ethically and in accordance with the following:
 
#### Unacceptable uses of generative AI tools
- Do not feed direct assignment/coursework questions into these tools and obtain the answers.
- Do not use these tools to generate content or solutions (words or source code) to direct questions in an assignment or any other coursework submission. This includes copying and pasting, or paraphrasing the answers produced by these tools.
- You also should not use these tools to generate summaries and rely upon them as a substitute for the original course materials.

#### Acceptable uses of generative AI tools
You may use AI tools to: 
- Explore a topic, find examples, definitions, limitations, etc.
- Brainstorm project/presentations ideas
- Create individual study guides
- Fix your grammar and style
- Proofread, paraphrase, or refine your original writing
- Code refactoring: refactor your original code.
- Debug and fix a bug introduced in your code

Unless granted a permission by the course instructor, any use of generative AI tools outside of acceptable uses in this course is considered a violation of academic integrity and will be subject to KAU's disciplinary actions.

#### Cite your use of generative AI tools
The acceptable use of generative AI tools must be cited properly just as in any other source. You're required to acknowledge your use of generative AI tools. For example, if you use ChatGPT-3, you must cite at the end of your work: *ChatGPT-3, DD/MM/YYYY* of the prompt*, "Full text of your prompt". Please keep the following in mind when using generative AI tools:
- If you copy verbatim from a generative AI tool, you must cite it with double quotation marks, indicating that the words used were not your own. Otherwise, you're considered cheating.
- If you paraphrase the output of a generative AI tool, you must cite it but not necessarily with double quotation marks, indicating that the idea, approach, format presented were not originally your own. Changing only a few words without citation is always considered plagiarism because the original ideas or data were presented without proper acknowledgment.

#### Check for correctness and accuracy
Generative AI tools are trained on imperfect data and are known for generating biased or factually incorrect output. You must not trust anything that these tools generate. If you have used them to explore a topic, and they returned a fact, date, or a number, assume it is wrong unless you either know the answer or can verify it with another source.

#### AI Detection

You must be transparent in how you used generative AI tools. If your work is suspected of not being original work, your course instructor may ask you to demonstrate your own understanding. This includes requiring you to do an oral presentation and asking you questions to demonstrate your authorship. Your course instructor may also use AI-detection tools provided by the university (e.g., Turnitin) that will generate an AI creation probability score, and if your work is flagged at a rate higher than 20% and you failed to demonstrate your authorship, then it's considered a violation of academic integrity and you will be subject to KAU's disciplinary actions.

## Students and Disability Accommodations
KAU welcomes students with disabilities into all of the University’s educational programs. If you are a student with special needs, your course instructor will work with the Special Needs Center Services at KAU's Deanship of Students' Affairs to provide you with reasonable and appropriate accommodations. This should be communicated as early in the semester as possible as the process of providing you with accommodations require administrative work.
