---
title: "Group Presentations and Demo | Fall 2018"
date: 2018-12-03T13:12:48+03:00
draft: false
showList: false
---


<div class="content">
<div class="columns is-multiline">
    <div class="column is-one-quarter-desktop is-half-tablet">
        <div class="card">
          <header class="card-header">
            <p class="card-header-title">
              Microsoft Azure Functions
            </p>
          </header>
          <div class="card-content">
            <div class="card-image">
                        <figure class="image is-3by2">
                            <img src="/images/presentations/fall-18/Microsoft-Azure-Functions.png" alt="">
                        </figure>
            </div>
            <p class="subtitle">
                <span class="tag is-medium">Mohammed Qara &</span>
                <span class="tag is-medium">Salem Abdulaziz</span>
            </p>
          </div>
            <footer class="card-footer">
                <a class="card-footer-item is-button" href="./azure-functions">
                    View
                </a>
            </footer>
        </div>
    </div>
    <div class="column is-one-quarter-desktop is-half-tablet">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                EFF Certbot
                </p>
            </header>
            <div class="card-content">
                <div class="card-image">
                    <figure class="image is-3by2">
                        <img src="/images/presentations/fall-18/eff-certbot.png" alt="">
                    </figure>
                </div>
                <p class="subtitle">
                    <span class="tag is-medium">Waleed Bin Saad &</span>
                    <span class="tag is-medium">Mouath Bahattab</span>
                </p>
            </div>
            <footer class="card-footer">
                <a class="card-footer-item" href="./eff-certbot">
                    View
                </a>
            </footer>
        </div>
    </div>
    <div class="column is-one-quarter-desktop is-half-tablet">
        <div class="card">
            <header class="card-header">
            <p class="card-header-title">
              Monit
            </p>
          </header>
            <div class="card-content">
                <div class="card-image">
                    <figure class="image is-3by2">
                        <img src="/images/presentations/fall-18/monit.png" alt="">
                    </figure>
                </div>
                <p class="subtitle">
                    <span class="tag is-medium">Fahad AlSayyali &</span>
                    <span class="tag is-medium">Osama AlSarhani</span>
                </p>
            </div>
            <footer class="card-footer">
                <a class="card-footer-item" href="./monit">
                    View
                </a>
            </footer>
        </div>
    </div>
        <div class="column is-one-quarter-desktop is-half-tablet">
            <div class="card">
                <header class="card-header">
                <p class="card-header-title">
                DataDog
                </p>
            </header>
                <div class="card-content">
                    <div class="card-image">
                        <figure class="image is-3by2">
                            <img src="/images/presentations/fall-18/datadog.png" alt="">
                        </figure>
                    </div>
                    <p class="subtitle">
                        <span class="tag is-medium">Anas Basalamah & </span>
                        <span class="tag is-medium"> Abdulaziz Alshememry</span>
                    </p>
                </div>
                <footer class="card-footer">
                    <a class="card-footer-item" href="./datadog">
                        View
                    </a>
                </footer>
            </div>
        </div>
        <div class="column is-one-quarter-desktop is-half-tablet">
            <div class="card">
                <header class="card-header">
                    <p class="card-header-title">
                    ELK Stack
                    </p>
                </header>
                <div class="card-content">
                    <div class="card-image">
                        <figure class="image is-3by2">
                            <img src="/images/presentations/fall-18/elk.svg" alt="ELK stack">
                        </figure>
                    </div>
                    <p class="subtitle">
                        <span class="tag is-medium">Mohammed Alghamdi &</span>
                        <span class="tag is-medium">Wael Alabarakati</span>
                    </p>
                </div>
                <footer class="card-footer">
                    <a class="card-footer-item" href="./elk">
                        View
                    </a>
                </footer>
            </div>
        </div>
            <div class="column is-one-quarter-desktop is-half-tablet">
                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title">
                        Netdata
                        </p>
                    </header>
                    <div class="card-content">
                        <div class="card-image">
                            <figure class="image is-3by2">
                                <img src="/images/presentations/fall-18/netdata.png" alt="Netdata">
                            </figure>
                        </div>
                            <p class="subtitle">
                                <span class="tag is-medium">Raad Alotaibi &</span>
                                <span class="tag is-medium">Yousef Alkhuzaie</span>
                            </p>
                    </div>
                    <footer class="card-footer">
                        <a class="card-footer-item" href="./netdata">
                            View
                        </a>
                    </footer>
                </div>
            </div>
</div>