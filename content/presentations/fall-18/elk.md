---
title: "ELK Stack"
date: 2018-12-20T13:12:48+03:00
toc: false
draft: false
---

By Mohammed Alghamdi & Wael Alabarakati 

## What is ELK Stack?
> The [ELK stack](https://www.elastic.co/elk-stack) is an amazing and powerful collection of three open source projects - Elasticsearch, Logstash, and Kiban.

<div>
<embed src="../files/ELK-STACK.pdf" type="application/pdf" width="960" height="569" >
<div>