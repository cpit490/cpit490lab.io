---
title: "Monit"
date: 2018-12-20T13:12:48+03:00
toc: false
draft: false
---

By Fahad AlSayyali & Osama AlSarhani 

## What is Monit?
> [Monit](https://mmonit.com/monit/) is a small Open Source utility for managing and monitoring UNIX systems. Monit conducts automatic maintenance, repair, and can execute meaningful causal actions in error situations.

<div>
<embed src="../files/monit.pdf" type="application/pdf" width="960" height="569" >
<div>

## Why Monit?
- For Files, Dirs, and File Systems:
You can use Monit to monitor files, directories and filesystems on localhost.
- For Programs and Scripts:
Monit can be used to test programs or scripts at certain times.
- For Processes:
You can use Monit to monitor daemon processes or similar programs running on localhost.
- For Cloud and Hosts:
Monitor network connections to various servers.
## Installing Monit

1. For installing Monit, you need to add the EPEL repository first:
     
     ```bash
    yum install epel-release
     ```
2. After that you can install Monit using:
    
    ```bash
    yum install monit
    ```
3. You can execute the following commands to start Monit service and make it run at startup and reload it:
    ```bash
    systemctl start monit
    systemctl enable monit
    systemctl restart monit
    ```
4. For starting Monit:
    ```bash
    monit
    ```
5. For showing the status:
    ```bash
    monit status
    ```

## Configuring Monit
- (/etc/monitrc) is the main configuration file of Monit. We will make a few changes to the Monit configuration file for our requirement:
    
    ```bash
    nano /etc/monitrc
    ```
- By default, Monit is set to check the services at an interval of 1 min. This setting can be altered by changing this line:
    
    ```
    set daemon 60
    ```

## Enable the web interface

- In the Monit global configuration file, scroll down to the following block:
    
    ```
    set httpd port 2812 and use address localhost 
    # only accept connection from localhost
    allow localhost 
    # allow localhost to connect to the server
    allow admin:monit 
    # require user 'admin' with password'monit'
    ```
- By default, Monit only listens on “localhost” and only answers to “localhost” as well, So if you want to be able to connect to Monit interface from anywhere, you have to comment the following lines:
    ```
    set httpd port 2812 and
    #use address localhost # only accept connection from localhost
    #allow localhost # allow localhost to connect to the server and
    allow admin:monit # require user 'admin' with password 'monit'
    ```
- Make sure to change your username and password to something strong and add this extra lines like below:
    ```
    set httpd port 2812
    allow 0.0.0.0/0.0.0.0
    allow admin:monit
    ```
- Now you can visit your Monit web interface at the following address: `http://IP_OR_DOMAIN:2812`

   
## Firewall

- Configure the firewall to allow access to Monit web interface, running on port 2812.
    
    ```bash
    firewall-cmd --permanent --add-port=2812/tcp
    firewall-cmd –reload
    ```