---
title: "Netdata"
date: 2018-12-20T13:12:48+03:00
toc: false
draft: false
---

## What is Netdata?

> [Netdata](https://my-netdata.io) is distributed, real-time, performance and health monitoring for systems and applications.

<div>
<embed src="../files/netdata.pdf" type="application/pdf" width="960" height="569" >
<div>

## How to install and run Netdata?

1. Update system to avoid any problems
    
    ```bash
    sudo yum update
    ```

2. Install netdata
    
    ```bash
    bash <(curl -Ss https://my-netdata.io/kickstart.sh)
    ```

3. Open the default 19999 tcp port in the cloud provider (e.g., Azure) portal
4. Open the same port using the firewall-cmd command below.
    ```bash
    sudo firewall-cmd --permanent --add-port=19999/tcp
    sudo firewall-cmd --reload
    ```

5. Done.
