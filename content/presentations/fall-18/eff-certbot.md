---
title: "Eff Certbot"
date: 2018-12-03T13:13:51+03:00
toc: false
draft: false
---
### Enable HTTPS on your website with EFF's Certbot

By Waleed Bin Saad & Mouath Bahattab

[EFF Certbot](https://certbot.eff.org/)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSAnzwe2c2O1U4Z0MpGewKhoZFub305uPgaEUcPicVsl5Ilf9o6Qu22J6hC1po-ZlUuC4R8oCtPYHqf/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="false" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Instructions

1. first enable the EPEL repository:

      ```bash
sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
      ```
2. enable the optional
   
      ```bash
sudo yum -y install yum-utils
sudo yum-config-manager --enable rhui-REGION-rhel-server-extras rhui-REGION-rhel-server-optional
      ```
3. install Certbot
       
      ```bash
sudo yum -y install python2-certbot-apache
      ```
4. virtual hosts replace "waleed0.me" with your domin
      
      ```bash
sudo nano /etc/httpd/conf.d/waleed0.me.conf
      ```  
      ```
    <VirtualHost *:80>
    ServerAdmin admin@waleed0.me
    DocumentRoot "/var/www/html/waleed0.me/"
    ServerName waleed0.me
    ServerAlias waleed0.me
    ErrorLog "/var/log/httpd/waleed0.me-error_log"
    CustomLog "/var/log/httpd/waleed0.me-access_log" combined

    <Directory "/var/www/html/waleed0.me/">
    DirectoryIndex index.html index.php
    Options FollowSymLinks
    AllowOverride All
    Require all granted
    </Directory>
    </VirtualHost>
      ```
         
      ```bash
    sudo mkdir /var/www/html/waleed0.me
    sudo chown apache:apache -R /var/www/html/waleed0.me/
      ```

5. Get Started
      
      ```bash
sudo certbot --apache
      ```
6. move index.html
      
      ```bash
cd /var/www/html/
sudo mv index.html ./waleed0.me/
      ```
