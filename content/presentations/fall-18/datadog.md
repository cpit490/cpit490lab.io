---
title: "Monit"
date: 2018-12-20T13:12:48+03:00
toc: false
draft: false
---

By Anas Basalamah & Abdulaziz Alshememry 

## What is DataDog?
> [Datadog](https://www.datadoghq.com/) is a monitoring service for your systems, applications and services. Datadog lets you collect all these metrics, events, and service states in one place. Then, visualize and correlate the data with beautiful graphs, and set flexible alerting conditions.

<div>
<embed src="../files/datadog.pdf" type="application/pdf" width="960" height="569" >
<div>