---
title: "Lab 4: Cloud Computing, Accessing a Virtual Private Server, and the Nginx web server"
date: 2018-09-01T16:39:02+03:00
weight: 4
draft: false
---


> Generate SSH key pair, create a VM instance, log in to the VM instance, install a web server, add a simple web page, view the web page in action, and destroy the instance.

### Video
> Note: The video below uses CentOS instead of Ubuntu Linux because at the time of recording CentOS was the operating system of choice for this course. However, these lab instructions assume that you're using Ubuntu Linux.

<iframe width="560" height="315" src="https://www.youtube.com/embed/g7tOVZ8HLxA?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

### Setup
- This lab assumes that you're using Bash Shell on both client and server.
  - Both Linux and macOS include a bash shell. For Windows, see [this lecture note on how to install a Bash shell on Windows](/notes/cloud-computing/#ssh-clients-and-terminal-console-emulators).
- This lab assumes that you have an account with a cloud provider (e.g., _Microsoft Azure_), see [this lecture note on how to create an account with _Microsoft Azure_ and other cloud providers](/notes/cloud-computing).
- You need to have a text editor (e.g., nano or vi) and be familiar with using it.

### Notes
- Below is a list of commands you will be using in these lab exercises. Make sure you read the manual of each one using the command `man`.
  - `ssh-keygen`
  - `ssh`


### Generating SSH keys with OpenSSH
1. Generate SSH key pair on your client machine
  - **Windows**:
    - Go to the Windows Start menu and search for *"Apps & Features"* **->** click *"Optional Features"*.
    - Scroll down the list to see if *"OpenSSH Client"* is listed.  If not, click the plus sign next to *"Add a feature"* **->** select *OpenSSH Client* **->** click *"Install"*.
  - **Mac**: 
    - Almost all devices running *macOS* should have openSSH installed by default. If for any reason you can't find it, use homebrew to install it using `brew install openssh`
  - **Linux:**
    - You should have OpenSSH installed on all major linux distributions. If you can't find it, use your distribution package manager to install or update it. For example, on ubuntu, you will run `sudo apt install openssh-client`.
2. Use `ssh-keygen` to generate SSH key pair (public and private keys) using RSA encryption and a bit length of 4096.       
    
    ```shell
    ssh-keygen -t rsa -b 4096 -C "azure-key"
    ```
    - You will be prompted to enter a path to save the keys and a passphrase. You will need to enter the passphrase every time you use the generated private key.
3. Either upload or copy the generated public key and add it to your cloud provider account.
      ```bash
      cat /path/to/your/public/key/file/your-public-key.pub
      ```
4. Log into your cloud provider account, create a new VM instance, and add the public key you copied in the previous step. In the case of _Microsoft Azure_, you will do:
     - Log in to the Azure portal at [http://portal.azure.com](http://portal.azure.com).
     - Click on **Virtual Machines** in the left-hand side bar of the Azure portal.
     - Click on **+ Add** 
     - In the **Basics tab**, you should have one subscription that is selected and then choose to _Create new_ under _Resource group_. In the pop-up, type any name, and then choose _OK_.
     - Under **Instance details**, type _my-ubuntu-VM-instance_ for the Virtual machine name, choose _East US_ for your Region, choose the recent **Ubuntu** image for the image, and the cheapest VM type (e.g., **Standard_B1S 1vCPU 1GiB memory**).
     - Under **Administrator account**, select SSH public key, and paste the public key you copied in the previous step. Alternatively, you may have Azure generates new key pair for you and downloading the private key on your client machine by selecting "Generate new key pair". 
     - Under **Inbound port rules", select the **"Public inbound ports** to open. select the radio button that says **"Allow selected ports"** and then select **SSH (22)** and **HTTP (80)** from the drop-down menu.
     - Click on **Next: Disks >** and select the default 30GiB Premium SSD disk.
     - Click **Next** and leave the default selections for **Networking**, **Management**, **Guest Config**, and **Tags**.
     - Click **Review + create** and wait for Azure to provision this VM
5. Connect to the VM instance using SSH
  - Click on the **Connect** button on the overview page for your VM.
  - You may access the VM instance using one of the following methods:
    - Using ssh command. You can copy and paste the command into your terminal.
    - Use the public ip and user name you chose when creating your VM and run:
    
       ```bash
       ssh -i /path/to/your/private/key/file azureuser@public-ip-or-DNS-name
       ```
    - You will prompted to enter the _passphrase_ for your VM instance.
    - Now you should be logged in to your remote VM instance.
    - If you're not logged in to your VM as a _root_, create a new user, and add it to the _wheel_ group.
6. Install the **nginx** web server on your VM. _nginx_ is not installed by default, so we ill use _apt_ to install it.
    
    ```bash
    sudo apt update
    sudo apt install nginx
    ```
7. Configure ufw firewall. Before we can use nginx, we need to add a rule to Ubuntu's default firewall, ufw, to enable outbound HTTP traffic (port 80 - unencrypted web traffic).
         
    ```bash
    sudo ufw app list
    ```
    ```
    Available applications:
      Nginx Full
      Nginx HTTP
      Nginx HTTPS
      OpenSSH
    ```

    ```perl
    sudo ufw allow 'Nginx HTTP'
    ```

8. Start _nginx_
 ```bash
   sudo systemctl start nginx
 ```
9. Open the public ip or domain of your VM in your browser. You should see something like:
    
    ![](/images/week-5/nginx-welcome-page.png)
10. Add a custom HTML page. 
   - Open the config file at `/etc/nginx/conf.d/default.conf` in your text editor:

     ```bash
     sudo nano /etc/nginx/conf.d/default.conf
     ```
   - Change the Path to _the root directory_ of your web server. The default value is `/usr/share/nginx/html` so change it to `/var/www/html`:
       
       ```
       location / {
         root /var/www/html;
       }
       ```    


   - Create a root directory that will contain our web pages.
      
      ```bash
      sudo mkdir -p /var/www/html
      ```
   - Create a simple **index.html** file inside that directory using your text editor (e.g., `sudo nano /var/www/html/index.html`) with the following content:
   
      ```html
      <!DOCTYPE html>
      <html lang="en">
        <head>
            <title>CPIT-490</title>
        </head>
        <body>
            <h1>Welcome to CPIT-490 website</h1>
        </body>
      </html>
      ```
11. Change the owner, permissions, and add firewall rules
  - We need to change the owner (chown) of our data directory to the user who runs the nginx server, which is usually **nginx**. We also need to change the permission to **755** for directories and **644** for files. These are the recommended permission bits for files served by a web server.
     
     ```bash
     sudo chown -R nginx:nginx /var/www/html
     sudo chmod -R 755 /var/www/html
     sudo chmod 644 /var/www/html/index.html
     ```
     > **Note:** The username used in the `chown` command should be the username that nginx runs with. You can check that by running the command: `ps -ef | grep nginx` and you should see that the first column of the _worker process_ shows **nginx** as the username. Alternatively, you can open the default config file at `/etc/nginx/nginx.conf` and you should see the value of the user directive as **nginx**.
     
11. Access the web page using the public IP address of your VM instance.
   - Obtain the public IP address or domain of your VM instance from the cloud provider's web portal.
   - Open your browser and visit `http://<your-vm-public-ip-address>/index.html`. You should see the web page you have created and served by nginx.
12. Clean up resources
  - Stop the nginx web server:
    
     ```bash
     sudo systemctl stop enginx
     ```
  - Delete VM instance, public IP, and disks from your account on the Azure portal.


## Submission
Submit your answers with screenshots showing the commands you executed as a PDF file by the due date.

<span class="tag is-info is-medium">Lab submissions are due one hour before the next week's lab begins.</span>
