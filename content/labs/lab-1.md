---
title: "Lab 1: Getting Started"
date: 2018-09-01T14:12:36+03:00
lastmod: 2022-12-10T09:30:51+03:00
weight: 1
draft: false
---

> Install a Linux system, log in, and run simple commands using the shell.

## Objectives
1. Setting up a working Linux environment.
2. Logging in, activating the user interface, and logging out.
3. Changing user passwords
4. Reading manuals

## Setup
- Download and install Oracle VM [VirtualBox](https://www.virtualbox.org/), a free and open-source hypervisor.
- Download and install [Ubuntu Server (the LTS server ISO file)](https://ubuntu.com/download/server) on VirtualBox.
  - [You may refer to this tutorial](https://ubuntu.com/tutorials/how-to-run-ubuntu-desktop-on-a-virtual-machine-using-virtualbox), which will walk you through the steps of installing Ubuntu virtual machine using VirtualBox. While the tutorial is for installing Ubuntu Desktop, the steps should be the same for installing Ubuntu Server.


## Lab Exercises

### 1. Basic info
Obtain the following results:
  - The name and version of the Operating System using the commands:
      ```bash
      lsb_release -a
      ```
      and

      ```bash
      uname -a
      ```
  - The logged in user name using the command `whoami`.
      ```bash
      whoami
      ```
  - The host name using the commands `hostname` and `hostnamectl`.
      ```bash
      hostname
      ```
      and

      ```bash
      hostnamectl
      ```

### 2. Navigating the filesystem
- Change directory `cd` to /root. What happens?
    ```bash
    cd /root
    ```

- Change directory `cd` to your home directory using `cd /home/username` (change username with the your actual username) or `cd ~`. The tilde (~) is a Linux "shortcut" to denote a user's home directory.
    ```bash
    cd /home/username
    cd ~
    ```
- Now `cd` to `..` . Use the `pwd` command. Where are you now?
    ```bash
    cd ..
    pwd
    ```
### 3. Getting help
#### man
- Read the manual for the __ls__ command using `man ls`.
    ```bash
    man ls
    ```
  - How to list hidden files?
  - How to display the output in a long listing format and print directory size info in human readable format (e.g., KB, MB, GB)?
  - Read the manual for the __apt__, the package manager for Ubuntu using `man apt`. What are some command options you can use with __apt__?

> The advanced package tool (apt), is a powerful tool that simplifies the process of installing and uninstalling software packages.

#### apropos
- The _apropos_ tool is particularly useful when searching for commands without knowing their exact names. apropos's syntax is: `apropos` followed by the search keywords.
  - The database searched by apropos is updated by  the  __mandb__  program, so you need to update it as a root user using `sudo mandb`, which will create or update the manual page index caches.

      ```bash
      sudo mandb
      ```
  
  - Search for _disk_ related tools using `apropos disk`:
    ```bash
    apropos disk
    ```
    - What is the name of the tool that gives you information about disk usage and space? If you have a problem, see the notes section below.
  - Use __apropos__ to find the editing programs that are available on your system? If you have a problem, see the notes section below.

> **Note:** if the command `apropos` returns _nothing appropriate_ on every search keyword you use or does not show recently installed software packages, then you'll have to run `mandb` as a root using `sudo mandb` and run _apropos_ again.
## Submission
Submit your answers with screenshots showing the commands you executed as a PDF file by the due date.

<span class="tag is-info is-medium">Lab submissions are due one hour before the next week's lab begins.</span>