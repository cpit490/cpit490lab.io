---
title: "Lab 8: Managed Databases and PaaS"
date: 2024-04-30T11:55:50+03:00
weight: 8
draft: true
---

> Deploy a web application in a platform as a service (PaaS)

In this lab, you will deploy a web application on a PaaS. The application monitors car prices by extracting data from a website, storing it in a database, and displaying the results in a web application.
You will deploy a web application that tracks car prices by scraping a website, storing it in a database, and showing the results in a web app. The application consists of the following:
 - Web scraper: This is a java spring web server
 - React application: this is the front end Java application
 - Database server: This is a NoSQL MongoDB database

 This is a common three-tier application, which is a widely used software design pattern for developing web applications. The three tiers are:

- Presentation Tier (Frontend): This is the topmost level of the application. In this case, it's the React application, which is responsible for presenting the user interface and capturing user interactions.

- Application Tier (Logic): This is the middle tier that contains the business logic of the application. Here, it's the Java Spring web server, which acts as a web scraper to extract data from a website.

- Data Tier (Backend): This is the bottom tier that stores and retrieves data. In this scenario, it's the NoSQL MongoDB database, which stores the scraped data.

Each tier is developed and maintained as an independent module, most often on separate platforms. This architecture provides developers the flexibility to modify or add services on a specific layer without affecting the other layers.


