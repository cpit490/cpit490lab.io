---
title: "Lab 3: Bash Scripting"
date: 2018-09-01T16:38:54+03:00
lastmod: 2022-12-30T16:38:54+03:00
weight: 3
draft: false
---


> Bash Scripting: redirection, pipes, basic bash scripting, executing scripts, conditional statements, repetitive statements, and environment variables.



<iframe width="560" height="315" src="https://www.youtube.com/embed/D9NEG9yigaY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

### Setup
- This lab assumes that you're using Bash Shell, the default shell on most systems.
- You need to have a text editor (e.g., nano, vi).

### Notes
- Redirection
  - _>_ is the output (stdout) redirection operator. _>>_ appends output to an existing file
  - _<_ is the input (stdin) redirection operator
  - _2>_ is the error (stderr) redirection operator
- Pipes use the symbol | and are used to connect the stdout of one command into the stdin of another command.
- Below is a list of commands you will be using in these lab exercises. Make sure you read the manual of each one using the command `man`.
  - `grep`
  - `wc`
  - `curl`
  - `du`
  - `history`
- If your date or time is incorrect, then you need to sync it with a tool like `ntpd`. [See how to install and run it here.](https://www.cyberciti.biz/faq/howto-install-ntp-to-synchronize-server-clock/)

### Exercises
1. Redirection
  - Use the _du_ tool to find the disk usage of each file at _/var/log_. Use the _-h_ option to show the size information in human-readable format (Kilobyte, Megabyte, Gigabyte, etc.): `du -h /var/log`
    - Re-run the command and redirect the output into a file named _log-size.txt_
    - Re-run the command and redirect the errors into a file named _error.log_.
    - Re-run the command to redirect the errors into a file named _error.log_ and the output into a file named _log-size.txt_.
  - Save the history of commands you previously executed since you logged in into a file named _mycommands.txt_. _Hint:_ use the `history` command and redirect its output.
2. Pipes
  - Count how many programs/commands in _/bin_. _Hint:_ You need to use the `ls` and `wc` commands.
  - Run `curl -h`. Find the command line option for including headers in the output. _Hint:_ pipe the output of `curl -h` into `grep`.


3. Basic Bash Scripting
  - Simple script
      - Write a shell script that creates a unique file.
          - _Hint:_ You may add the timestamp to the end of the file name, which can be obtained using `date +%s`.
      - Change permissions on your script so that you can run it.
      - Run the script in normal mode. It should run without errors. 
  - Running a remote script:
      - Download the [following script](https://gitlab.com/cpit490/examples/raw/master/hello.sh) using _curl_ and run it remotely:
          - Option 1: Run the script directly by piping its content from curl into bash.
            - `curl https://gitlab.com/cpit490/examples/raw/master/hello.sh | bash -`
              - **Note:**  This is obviously considered a bad idea for security reasons, so you should check always the content of the script before executing it.
          - Option 2: Download the file locally, change its permission, and execute it:
             
              ```bash
              curl -O https://gitlab.com/cpit490/examples/raw/master/hello.sh
              chmod u+x hello.sh
              ./hello.sh
              ```
  - Conditional execution
      - Use the logical operator `&&` to control the sequence of command execution. This is useful when you want to run the second command only if the first command is successful.
          - `ls ~/bin && cd ~/bin`

4. Conditional statements in Bash
  - Download this [shell script](https://gitlab.com/cpit490/examples/raw/master/if-statements.sh) using `curl -O https://gitlab.com/cpit490/examples/raw/master/if-statements.sh`. This script checks for IPv4 connectivity using `ping` and displays an error message if the connection is down.
     - Run the script.
     - Edit the script, so it asks the user if he wants to re-run ping and save the output into a file. Example:
         
         ```bash
         ./if-statements.sh
         ```
         ```console
         This script checks for IPv4 connectivity
         ping: sendto: No route to host
         Network is down
         Would you like to check again and save the output into a file? [y/n]:y
         ping: sendto: No route to host
         ```

5. Repetitive statements in Bash
  - Fix the following script that should display the size of the home directory for each bash user in the system:
            
      ```
      # Loop through the list of directories in /home/*
      for ...
        du -h -d 0 $userDir
      done
      ```

6. Environment Variables
  - Rename the script in step # 4 from `if-statements.sh` into `check-network.sh` and add it to a directory called _bin_ under your home directory (`~/bin/`).
    - Open the file `~/.bash_profile` in your text editor (e.g., `nano ~/.bash_profile`).
    - Append the path to `~/bin` to the environment variable $PATH. This means you will go to the end of the file and add this line `PATH=$PATH:/home/<your-user-name>/bin`.
    - Save it and reload it by running: `source ~/.bash_profile`.
    - Move into a different directory (e.g., `cd ~/`) and run the script by its name only: `check-network.sh`

## Submission
Submit your answers with screenshots showing the commands you executed as a PDF file by the due date.

<span class="tag is-info is-medium">Lab submissions are due one hour before the next week's lab begins.</span>
