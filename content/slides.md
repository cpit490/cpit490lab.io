---
title: "Lecture Slides"
date: 2024-03-24T09:37:27+03:00
draft: false
---

- [Week 1 slides: Overview of System Administration](https://kaucpit490.github.io/slides/decks/week-01/)
- [Week 2: The UNIX/Linux Filesystem](https://kaucpit490.github.io/slides/decks/week-02/)
- [Week 3: Scripting and the Shell](https://kaucpit490.github.io/slides/decks/week-03/)
- [Week 4-5 : Cloud Computing Fundamentals](https://kaucpit490.github.io/cloud-computing-slides/)
- [Week 6-8: Cloud Computing Networking](https://kaucpit490.github.io/cloud-networking-slides/)
- [Week 9-10: Virtualization and Containers](https://kaucpit490.github.io/virtualization-slides/)
