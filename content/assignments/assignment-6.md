---
title: "Assignment 6"
date: 2022-12-28T09:44:53+03:00
weight: 6
draft: false
---

You need to create a simple web page, serve it using the apache web server, and map a domain name to your server public IP address.

You are asked to set up DNS aliases with the Apache web server. First, you need to obtain a domain name from a domain name registrar. You may obtain a free domain through [GitHub student pack](https://education.github.com/pack) or [others](https://www.google.com/search?hl=en&q=free%20domain%20provider). Second, you need to set up and manage the DNS records for your domain by using a DNS hosting service such as [Azure](https://azure.microsoft.com/en-us/services/dns/) or [the one provided by domain name registrar](https://www.namecheap.com/support/knowledgebase/article.aspx/768/10/how-do-i-register-personal-nameservers-for-my-domain), where you simply enter the nameserver name and the public IP address of your VM instance through the dashboard of the DNS hosting service.


Submit a PDF file that contains the link to your domain and screenshots of the steps you followed to register a domain name and map it to an instance.

> Assignments are due at the beginning of the following week one hour before the class. A submission will be labeled late when it has been submitted past the due date.
