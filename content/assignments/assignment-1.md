---
title: "Assignment 1"
date: 2022-12-12T22:54:32+03:00
weight: 1
draft: false
---


Follow the steps in [User and Software Management](/notes/user-software-management/) and [The Filesystem](/notes/filesystem) and execute all the commands in these two note articles.

Submit a PDF file that contains screenshots of your system showing the execution of the commands and the output.

> Assignments are due at the beginning of the following week one hour before the class. A submission will be labeled late when it has been submitted past the due date.
