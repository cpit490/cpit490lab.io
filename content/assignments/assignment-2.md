---
title: "Assignment 2"
date: 2022-12-15T22:54:35+03:00
weight: 2
draft: false
---

Write a Bash script for adding a new user to your system. Your script should do the following:

- Prompt the user to enter the user name.
- Create a new user using `useradd`.
- Prompt the user to enter a strong password (8 characters or more) and set it using `passwd`.
- Prompt the user to enter a default group name.
- Append the entered group name to the user groups using `usermod -aG <groupname> <username>`.
- If there's an error during the previous step, the script should remain in the same step and not proceed to the next step.
- At the end, the script should ask the user to confirm creating the new user with a message. If the user decided not to create the user, delete it using `userdel -r <user_name>`.

Your script should make use of "_if statements_" and "_functions_".

Submit a PDF file that contains your bash script and screenshots of your system showing the execution of the script and the output.

> Assignments are due at the beginning of the following week one hour before the class. A submission will be labeled late when it has been submitted past the due date.
