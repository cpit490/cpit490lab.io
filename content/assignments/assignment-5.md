---
title: "Assignment 5"
date: 2022-12-31T09:44:53+03:00
weight: 5
draft: false
---

The goal of this assignment is to learn how to deploy an application on the cloud using three cloud deployment models:
- Infrastructure as a service (IaaS)
- Platform as a service (PaaS)
- Serverless

You're asked to deploy a REST API on three cloud deployment models: IaaS (Virtual Machines), PaaS, and serverleess. You may use an API you have developed or used. If you can't find an API to use and deploy, then you may use this [sentiment analysis API](https://gitlab.com/cpit490/sentiment-analysis-api), which is written in Python.

Compare the advantages and disadvantages of each deployment model and discuss which one may be the appropriate model for your API.

> Assignments are due at the beginning of the following week one hour before the class. A submission will be labeled late when it has been submitted past the due date.
