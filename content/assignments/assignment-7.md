---
title: "Assignment 7"
date: 2022-12-30T09:44:40+03:00
weight: 7
draft: false
---

Create a custom virtual machine using Packer and convert it into a Vagrant Box, then share it and distribute it on [Vagrant cloud](https://www.vagrantup.com/docs/vagrant-cloud/boxes/distributing.html)

Submit a PDF file that contains your Vagrantfile and screenshots of your system showing the steps you followed to distribute your Vagrant box.

> Assignments are due at the beginning of the following week one hour before the class. A submission will be labeled late when it has been submitted past the due date.