---
title: "Assignment 3"
date: 2022-12-16T22:54:38+03:00
weight: 3
draft: false
---

Install a PHP web app called [Kanboard](https://github.com/kanboard/kanboard) on a remote VM instance on the cloud provider of your choose (e.g., Azure). You need to do the following:

- Download [the latest release of the app](https://github.com/kanboard/kanboard/releases) (the tar.gz file).
- Extract the tar file using `tar -xvzf /path/to/the/file.tar.gz`.
- Check the [requirements](https://docs.kanboard.org/en/latest/admin_guide/requirements.html)
- Install the appropriate software version of PHP, MariaDB, and the Apache HTTP.
- Follow the installation instructions.
- Access the app using the public IP address of your VM instance.

Submit a PDF file that contains screenshots of your system showing the execution of commands and the output.

> Assignments are due at the beginning of the following week one hour before the class. A submission will be labeled late when it has been submitted past the due date.
