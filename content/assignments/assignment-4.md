---
title: "Assignment 4"
date: 2022-12-20T22:54:40+03:00
weight: 4
draft: false
---

Install and configure a content management system (CMS) called [Drupal](https://www.drupal.org/) on two VM instances on the cloud provider of your choose (e.g., Azure). The first instance should host the web server, and the second instance should host the database server. After installing Drupal, you should replace the default theme with a new one ([see the instructions here](https://www.drupal.org/docs/7/extend/installing-themes)).

Submit a PDF file that contains your bash script and screenshots of your system showing the execution of the script and the output.

> Assignments are due at the beginning of the following week one hour before the class. A submission will be labeled late when it has been submitted past the due date.